import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUsers, searchUser } from "../actions/userActions";
import Modal from "../UI/Modal";
import UserItem from "./UserItem";
import classes from "./User.module.css";
const User = () => {
  const [open, setOpen] = useState(false);
  const [text, setText] = useState("");
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users);
  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);
  const handleClose = () => setOpen(false);
  const handleSearch = (e) => {
    setText(e.target.value);
    dispatch(searchUser(text));
  };
  return (
    <div className={classes.userContainer}>
      <input
        type="text"
        placeholder="Search.."
        name="search"
        onChange={handleSearch}
        value={text}
      />

      <button type="submit" onClick={() => dispatch(searchUser(text))}>
        <i class="fa fa-search"></i>
      </button>
      <div>
        <h4>Users</h4>
        <span onClick={() => setOpen(true)}>
          <i class="fa fa-plus-circle" aria-hidden="true"></i>
        </span>
      </div>
      {open && <Modal handleClose={handleClose} open={open} />}
      {users &&
        users.userDetails.map((userDetail) => (
          <UserItem key={userDetail.id} {...userDetail} />
        ))}
    </div>
  );
};

export default User;
