import { useDispatch } from "react-redux";
import { gettingUserMessage } from "../actions/userActions";
import classes from "./UserItem.module.css";

const UserItem = (props) => {
  const dispatch = useDispatch();
  return (
    <>
      <div
        className={`${classes.userItemContainer} `}
        onClick={() => {
          dispatch(gettingUserMessage(props.id));
        }}
      >
        <div className={classes.userIcon}>
          <i class="fa fa-user" aria-hidden="true"></i>
        </div>
        <h6>{props.username}</h6>
      </div>
      <div className={classes.showOnHover}></div>
    </>
  );
};

export default UserItem;
