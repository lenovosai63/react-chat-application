import React, { useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import classes from "./DisplayMessage.module.css";
const DisplayMessage = () => {
  const users = useSelector((state) => state.users);
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };
  useEffect(() => {
    scrollToBottom();
  }, [users]);
  return (
    <>
      <div className={classes.userContainer}>
        {users &&
          users.messages &&
          users.messages.message &&
          users.messages.message.map((message1) => (
            <div className={classes.userItemContainer} key={message1.id}>
              <div className={classes.userIcon}>
                <i class="fa fa-user" aria-hidden="true"></i>
              </div>
              <div className={classes.messageBox}>{message1}</div>
            </div>
          ))}
        <div ref={messagesEndRef} />
      </div>
    </>
  );
};

export default DisplayMessage;
