import { useState } from "react";
import { sendingMessage } from "../actions/userActions";
import classes from "./SendMessage.module.css";
import { useDispatch } from "react-redux";

const SendMessage = () => {
  const [text, setText] = useState("");
  const dispatch = useDispatch();
  const textHandler = () => {
    if (text) {
      dispatch(sendingMessage(text));
      setText("");
    } else {
      alert("Please send some data...");
    }
  };
  return (
    <>
      <div className={classes.sendMessageContainer}>
        <h6 className="">New Message</h6>
        <div className={classes.horizontal}></div>
      </div>
      <div className={classes.sendMessage}>
        <input
          className={classes.input}
          type="text"
          placeholder="Type Something ...."
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <div className={classes.customButton}>
          <button onClick={textHandler}>Send </button>
          <i class="fa fa-paper-plane" aria-hidden="true"></i>
        </div>
      </div>
    </>
  );
};

export default SendMessage;
