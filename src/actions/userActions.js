import {
  ADD_USER,
  GET_USER_MESSAGE,
  SEARCH_USER,
  SEND_MESSAGE,
  USER_FAILURE,
  USER_REQUEST,
  USER_SUCCESS,
} from "../constants/userconstants";
import axios from "axios";

export const getUsers = () => async (dispatch, state) => {
  try {
    dispatch({
      type: USER_REQUEST,
    });
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/users"
    );
    dispatch({
      type: USER_SUCCESS,
      payload: data,
    });
  } catch (error) {
    console.log(error, "error");
    dispatch({
      type: USER_FAILURE,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const gettingUserMessage = (id) => (dispatch) => {
  dispatch({
    type: GET_USER_MESSAGE,
    payload: id,
  });
};
export const sendingMessage = (text) => (dispatch) => {
  dispatch({
    type: SEND_MESSAGE,
    payload: text,
  });
};
export const addUser = (username) => (dispatch) => {
  dispatch({
    type: ADD_USER,
    payload: username,
  });
};
export const searchUser = (username) => (dispatch) => {
  if (username.length > 1) {
    dispatch({
      type: SEARCH_USER,
      payload: username,
    });
  } else {
    dispatch(getUsers());
  }
};
