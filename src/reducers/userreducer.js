import {
  ADD_USER,
  GET_USER_MESSAGE,
  SEARCH_USER,
  SEND_MESSAGE,
  USER_FAILURE,
  USER_REQUEST,
  USER_SUCCESS,
} from "../constants/userconstants";
const initialState = {
  userDetails: [],
  messages: {},
};

export const userreducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case USER_SUCCESS:
      const userDetailsData = payload.map((userdetail) => ({
        messages: [
          `hii ${userdetail.name}`,
          `how are you ${userdetail.name}`,
          "Welcome to chat",
          "I am super Exicted",
          "How the life is going on",
          "And how they are",
          "How is the situation Outside?",
        ],
        ...userdetail,
      }));
      return {
        ...state,
        loading: false,
        userDetails: userDetailsData,
      };
    case USER_FAILURE:
      return state;
    case GET_USER_MESSAGE:
      const singleUserMessage = state.userDetails.find(
        (userDetail) => userDetail.id === payload
      );
      return {
        ...state,
        messages: {
          id: singleUserMessage.id,
          message: singleUserMessage.messages,
        },
      };
    case SEND_MESSAGE:
      const currentMessageID = state.messages.id;
      const updateMessages = state.userDetails.map((Userdetail) =>
        Userdetail.id === currentMessageID
          ? { ...Userdetail, messages: Userdetail.messages.concat(payload) }
          : Userdetail
      );
      return {
        ...state,
        userDetails: updateMessages,
        messages: {
          ...state.messages,
          message: state.messages.message && [
            ...state.messages.message,
            payload,
          ],
        },
      };
    case ADD_USER:
      return {
        ...state,
        userDetails: [
          {
            username: payload,
            id: Math.random(),
            messages: [
              `hii ${payload}`,
              `how are you ${payload}`,
              "Welcome to chat",
            ],
          },
          ...state.userDetails,
        ],
      };
    case SEARCH_USER:
      const filtered = state.userDetails.filter((userdetail) =>
        userdetail.username.includes(payload)
      );
      return {
        ...state,
        userDetails: filtered,
      };
    default:
      return state;
  }
};
