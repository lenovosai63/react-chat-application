import Header from "./UI/Header";
import { Provider } from "react-redux";
import classes from "./App.module.css";
import User from "./Components/User";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { store } from "./store";
import { Grid } from "@material-ui/core";
const App = () => {
  return (
    <Provider store={store}>
      <div className={classes.container}>
        <div className={classes.board}>
          <Grid container>
            <Grid item xs={4}>
              <User />
            </Grid>
            <Grid item xs={8}>
              <Header />
            </Grid>
          </Grid>
        </div>
      </div>
    </Provider>
  );
};

export default App;
