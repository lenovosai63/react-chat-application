export const USER_REQUEST = "USER_REQUEST";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_FAILURE = "USER_FAILURE";
export const GET_USER_MESSAGE = "GET_USER_MESSAGE";
export const SEND_MESSAGE = "SEND_MESSAGE";
export const ADD_USER = "ADD_USER";
export const SEARCH_USER = "SEARCH_USER";
