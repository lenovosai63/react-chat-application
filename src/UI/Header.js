import React from "react";
import SendMessage from "../Components/SendMessage";
import DisplayMessage from "../Components/DisplayMessage";
import classes from "./Header.module.css";
const Header = () => {
  return (
    <div className={classes.headerContainer}>
      <header>
        <h5>Chat Messages</h5>
      </header>
      <DisplayMessage />
      <SendMessage />
    </div>
  );
};

export default Header;
