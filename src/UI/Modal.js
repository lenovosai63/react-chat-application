import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addUser } from "../actions/userActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  Container,
  DialogActions,
  DialogTitle,
  makeStyles,
  TextField,
} from "@material-ui/core";
const useStyles = makeStyles({});
const Modal = ({ handleClose, open }) => {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const dispatch = useDispatch();
  return (
    <>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        className={classes.root}
        maxWidth="xs"
        fullWidth
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add Username
        </DialogTitle>
        <Container>
          <TextField
            type="text"
            required
            label="Enter Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Container>
        <DialogActions>
          <Button
            onClick={() => {
              if (username) {
                dispatch(addUser(username));
                handleClose();
              } else {
                alert("please enter username....");
              }
            }}
            color="primary"
            variant="contained"
          >
            Add Username
          </Button>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Modal;
